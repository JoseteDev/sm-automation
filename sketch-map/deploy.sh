#!/bin/bash

ENVIRONMENT=$1

case $ENVIRONMENT in
		"Production")
    	echo -n "Deploying Production env!"
    	docker-compose -f production.yml up -d --no-recreate
    ;;
		"Beta")
    	echo -n "Deploying Beta env!"
    	docker-compose -f beta.yml up -d --no-recreate
    ;;
    "API-Testing")
    	echo -n "Deploying API Testing env!"
				docker-compose -f beta.yml up -d --no-recreate
        docker-compose -f api-test.yml up -d
    ;;
    "Web-Testing")
    	echo -n "Deploying Web Testing env!"
				docker-compose -f beta.yml up -d --no-recreate
        docker-compose -f web-test.yml up -d
    ;;
    *)
    	echo -n "Unknown environment!"
    ;;
esac
